export class ContactFormMessage {
    name: string;
    email: string;
    phone: string;
    message: string;

    constructor() {
        this.name = '';
        this.email = '';
        this.phone = '';
        this.message = '';
    }
}