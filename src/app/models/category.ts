export interface Category {
  id: number;
  name: string;
  seo_url: string;
  photo: string;
  prefix: string;
}
