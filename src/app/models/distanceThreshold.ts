export interface DistanceThreshold {
  from: number;
  to: number;
  price: number;
}
