import { Category } from './category';
import { KeyValuePair } from './helpers/key-value-pair';
import { Textile } from './textile';
import { Addition } from './addition';

export interface Product {
    id: number;
    name: string;
    seoUrl: string;
    price: number;
    category: Category;
    description: string;
    parameters?: KeyValuePair[];
    sizes?: KeyValuePair[];
    attributes?: string[];
    images?: string[];
    textilies?: Textile[];
    additions?: Addition[];
}
