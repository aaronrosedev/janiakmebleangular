import { Product } from './product';
import { Textile, TextileColor } from './textile';
import { Addition } from './addition';
import { DistanceThreshold } from './distanceThreshold';

export interface Order {
  currentStep: number;
  product?: Product;
  textile?: Textile;
  delivery?: DistanceThreshold;
  textileColor?: TextileColor;
  additions?: Array<Addition>;
}
