export interface Textile {
    name: string;
    additionalPayment: number;
    samples: TextileColor[];
}

export interface TextileColor {
    title: string;
    url: string;
}
