export class Addition {
    id: number;
    name: string;
    price: number;
    description: string;
    image: string;
}