export interface ProductInCategory {
    id: number;
    name: string;
    price: number;
    img: string;
    isMainProduct: boolean;
    variants?: Array<ProductInCategory>;
    seoUrl: string;
}

