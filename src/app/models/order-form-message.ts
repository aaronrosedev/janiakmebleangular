export class OrderFormMessage {
    email: string;
    phone: string;
    message: string;
    product: string;

    constructor(product: string) {
        this.email = '';
        this.phone = '';
        this.message = '';
        this.product = product;
    }
}