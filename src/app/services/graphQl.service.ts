import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GraphQlService {

    public constructor(private http: HttpClient) { }

    public async SendQyery(query: string, resultNode: string): Promise<any> {

        const graphQlHost = 'https://graphql.datocms.com/';
        const token = 'a26046dc11e255575610f2c05acb6a';

        const data = JSON.stringify({
            query
        });

        const headers = {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${token}`
        };

        const response = await this.http.post<any>(graphQlHost, data, { headers }).toPromise();
        const object = response.data[resultNode];

        return object;
    }

}
