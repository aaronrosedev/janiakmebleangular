import { Textile } from './../models/textile';
import { Product } from './../models/product';
import { KeyValuePair } from '../models/helpers/key-value-pair';
import { Injectable } from '@angular/core';
import { GraphQlService } from './graphQl.service';
import { ProductInCategory } from '../models/product-in-category';

@Injectable()
export class ProductsService {

    public constructor(
        private readonly gqlService: GraphQlService
    ) { }

    public async getProductsInCategory(_categoryId: string): Promise<ProductInCategory[]> {
        const query1 = `{
                category(filter: {seoUrl: {eq: "${_categoryId}"}}) {
                  id
                }
              }`;

        const categoryFromDato = await this.gqlService.SendQyery(query1, 'category');

        const query2 = `{
            allProducts(filter: {category: {eq: ${categoryFromDato.id}}}) {
                id
                name
                seoUrl
                price
                photos {
                  url
                }
            }
          }`;

        const result = await this.gqlService.SendQyery(query2, 'allProducts');

        const productsFromDatoCMS = result.map(prod => ({
            id: prod.id,
            img: prod.photos[0].url,
            isMainProduct: true,
            name: prod.name,
            price: prod.price,
            seoUrl: prod.seoUrl

        }));

        return productsFromDatoCMS.sort((a, b) => a.name.localeCompare(b.name));
    }

    public async getRandomProducts(_count: number): Promise<ProductInCategory[]> {
        const query = `{
            allProducts {
                id
                name
                seoUrl
                price
                photos {
                  url
                }
            }
          }`;

        const result = await this.gqlService.SendQyery(query, 'allProducts');

        const productsFromDatoCMS = result.map(prod => ({
            id: prod.id,
            img: prod.photos[0].url,
            isMainProduct: true,
            name: prod.name,
            price: prod.price,
            seoUrl: prod.seoUrl

        }));

        return productsFromDatoCMS.sort((a, b) => 0.5 - Math.random()).slice(0, _count);
    }

    public async getProduct(_productId: string): Promise<Product> {
        const query1 = `{
                product(filter:{
                seoUrl: {eq:"${_productId}"}
                })
                  {
                    id
                    seoUrl
                    name
                    description
                    price
                    photos
                    {
                      url
                    }
                    category{
                      name
                      id
                      seoUrl
                      prefix
                      image {
                        url
                      }
                    }
                  }
                }`;

        const prod = await this.gqlService.SendQyery(query1, 'product');

        return {
            id: prod.id,
            seoUrl: prod.seoUrl,
            name: prod.name,
            price: prod.price,
            description: prod.description,
            images: prod.photos.map(img => img.url),
            category: {
                id: prod.category.id,
                name: prod.category.name,
                photo: prod.category.image.url,
                prefix: prod.category.prefix,
                seo_url: prod.category.seoUrl
            }
        };
    }

    public async getParametersToProduct(_productId: number): Promise<KeyValuePair[]> {
        const query1 = `{
                product(filter:{
                id: {eq:"${_productId}"}
                })
                  {
                    parameters
                  }
                }`;

        const prod = await this.gqlService.SendQyery(query1, 'product');

        return prod.parameters.split('\n').map(param => {
            const c = param.split(':');

            if (!c) {
                return null;
            }

            return new KeyValuePair(c[0].trim(), c[1].trim());
        });
    }

    public async getSizesToProduct(_productId: number): Promise<KeyValuePair[]> {
        const query1 = `{
                product(filter:{
                id: {eq:"${_productId}"}
                })
                  {
                    sizes
                  }
                }`;

        const prod = await this.gqlService.SendQyery(query1, 'product');

        return prod.sizes.split('\n').map(param => {
            const c = param.split(':');

            if (!c) {
                return null;
            }

            return new KeyValuePair(c[0].trim(), c[1].trim());
        });
    }

    public async getAttributesToProduct(_productId: number): Promise<string[]> {
        const query1 = `{
                product(filter:{
                id: {eq:"${_productId}"}
                })
                  {
                    productAttributes {
                        value
                    }
                  }
                }`;

        const prod = await this.gqlService.SendQyery(query1, 'product');

        return prod.productAttributes.map(att => att.value);
    }

    public getTextilies(): Promise<Textile[]> {
        const query = `{
            allTextilies {
              name
              additionalPayment
              samples {
                url
                title
              }
            }
          }`;

        return this.gqlService.SendQyery(query, 'allTextilies');
    }

}
