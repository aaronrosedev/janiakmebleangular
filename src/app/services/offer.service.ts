import { ProductsService } from './products.service';
import { CategoriesService } from './categories.service';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Category } from '../models/category';
import { ProductInCategory } from '../models/product-in-category';
import { KeyValuePair } from '../models/helpers/key-value-pair';
import { Product } from '../models/product';
import { Textile } from '../models/textile';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class OfferService {

    private productSizesObs = new BehaviorSubject<KeyValuePair[]>([]);
    private productParametersObs = new BehaviorSubject<KeyValuePair[]>([]);
    private productAttributessObs = new BehaviorSubject<string[]>([]);
    private productTextilesObs = new BehaviorSubject<Textile[]>([]);

    private readonly HOST = 'https://janiakmebleapi.herokuapp.com';

    private categories: Category[];

    private currentProduct: KeyValuePair;

    public constructor(
        private readonly http: HttpClient,
        private readonly categoriesService: CategoriesService,
        private readonly productsService: ProductsService
    ) { }

    public async getCategory(categoryName: string): Promise<Category> {
        const categories = await this.getCategories();

        return categories.find(cat => cat.seo_url === categoryName);
    }

    public async getCategories(): Promise<Category[]> {
        if (!this.categories) {
            this.categories = await this.categoriesService.getCategories();
        }

        return this.categories;
    }

    public getProductsInCategory(categoryName: string): Promise<ProductInCategory[]> {
        return this.productsService.getProductsInCategory(categoryName);
    }

    public getRecommendedProducts(count: number): Promise<ProductInCategory[]> {
        return this.productsService.getRandomProducts(count);
    }

    public async getProduct(productName: string): Promise<Product> {
        try {
            const result = await this.productsService.getProduct(productName);
            this._getSizes(result.id);
            this._getParameters(result.id);
            this._getAttributes(result.id);

            return result;
        }
        catch {
            return null;
        }
    }

    public getSizes(): Observable<KeyValuePair[]> {
        return this.productSizesObs.asObservable();
    }

    private _getSizes(productId: number): void {
        this.productsService.getSizesToProduct(productId).then(sizes => this.productSizesObs.next(sizes));
    }

    public getParameters(): Observable<KeyValuePair[]> {
        return this.productParametersObs.asObservable();
    }

    private _getParameters(productId: number): void {
        this.productsService.getParametersToProduct(productId).then(parameters => this.productParametersObs.next(parameters));
    }

    public getAttributes(): Observable<string[]> {
        return this.productAttributessObs.asObservable();
    }

    private _getAttributes(productId: number): void {
        this.productsService.getAttributesToProduct(productId).then(attributes => this.productAttributessObs.next(attributes));
    }

    public getTextilesObs(): Observable<Textile[]> {
        return this.productTextilesObs.asObservable();
    }

    public getTextiles(): void {
        this.productsService.getTextilies().then(textiles => this.productTextilesObs.next(textiles));
    }
}
