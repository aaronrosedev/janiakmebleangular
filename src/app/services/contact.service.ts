import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContactFormMessage } from '../models/contact-form-message';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { OrderFormMessage } from '../models/order-form-message';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private readonly HOST = 'https://janiakmebleapi.herokuapp.com';

  constructor(private http: HttpClient) { }

  public sendMessage(message: ContactFormMessage): Observable<Boolean> {
    // console.log('wysylam');
    return this.http.post<Boolean>(this.HOST + '/contact', message);
  }

  public sendOrderMessage(message: OrderFormMessage): Observable<Boolean> {
    //console.log('wysylam');
    return this.http.post<Boolean>(this.HOST + '/contact/order', message);
  }
}
