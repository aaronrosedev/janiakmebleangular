import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DistanceService {

  private _url = 'https://www.mapquestapi.com/directions/v2/route?key=8zS6GyiAAcDCu2VmO85UyACLFJvKyGf6&from=Twardog%C3%B3ra&to=##DESTINATION##&outFormat=json&ambiguities=ignore&routeType=fastest&doReverseGeocode=false&enhancedNarrative=false&avoidTimedConditions=false';

  constructor(private http: HttpClient) { }

  public getDistance(city: string): Observable<any> {
    return this.http.get(this._url.replace('##DESTINATION##', encodeURIComponent(city)));
  }
}


