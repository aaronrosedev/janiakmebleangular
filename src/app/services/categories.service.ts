import { Injectable } from '@angular/core';
import { Category } from '../models/category';
import { GraphQlService } from './graphQl.service';

@Injectable()
export class CategoriesService {

    public constructor(private datoService: GraphQlService) { }

    public async getCategories(): Promise<Category[]> {
        const query = `{
            allCategories{
              id
              name
              image {
                url
              }
              seoUrl
              prefix
            }
          }`;

        const result = await this.datoService.SendQyery(query, 'allCategories');

        return result.map(cat => ({
            id: cat.id,
            name: cat.name,
            photo: cat.image.url,
            prefix: cat.prefix,
            seo_url: cat.seoUrl
        }));
    }

}
