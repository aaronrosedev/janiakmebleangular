import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { BehaviorSubject, Observable } from 'rxjs';
import { Textile, TextileColor } from '../models/textile';
import { Order } from '../models/order';
import { Addition } from '../models/addition';
import { DistanceThreshold } from '../models/distanceThreshold';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private orderObs = new BehaviorSubject<Order>(null);

  constructor() { }

  public selectProduct(selectedProduct: Product) {
    let currentOrder = this.orderObs.value;

    if (!currentOrder) {
      currentOrder = { currentStep: 2 };
    }

    currentOrder.product = selectedProduct;
    this.orderObs.next(currentOrder);
  }

  public selectTextile(selectedTextile: Textile, selectedTextileColor: TextileColor) {
    this.setOrder('textile', selectedTextile);
    this.setOrder('textileColor', selectedTextileColor);
  }

  public selectDeliveryCost(delivery: DistanceThreshold) {
    this.setOrder('delivery', delivery);
  }

  public addAddition(addition: Addition) {
    const currentOrder = this.orderObs.value;

    if (!currentOrder) {
      return;
    }

    if (!currentOrder.additions) {
      currentOrder.additions = [];
    }

    currentOrder.additions.push(addition);

    this.orderObs.next(currentOrder);
  }

  public removeAddition(addition: Addition) {
    const currentOrder = this.orderObs.value;

    if (!currentOrder) {
      return;
    }

    if (!currentOrder.additions) {
      currentOrder.additions = [];
    }

    const index = currentOrder.additions.indexOf(addition);
    currentOrder.additions.splice(index, 1);

    this.orderObs.next(currentOrder);
  }

  public nextStep() {
    const currentOrder = this.orderObs.value;

    if (!currentOrder || currentOrder.currentStep >= 6) {
      return;
    }

    currentOrder.currentStep++;
    this.orderObs.next(currentOrder);
  }

  public prevStep() {
    const currentOrder = this.orderObs.value;

    if (!currentOrder || currentOrder.currentStep <= 2) {
      return;
    }

    currentOrder.currentStep--;
    this.orderObs.next(currentOrder);
  }

  public getOrderObs(): Observable<Order> {
    return this.orderObs.asObservable();
  }

  private setOrder(key: string, value: any) {
    let currentOrder = this.orderObs.value;

    if (!currentOrder) {
      currentOrder = { currentStep: 2 };
    }

    currentOrder[key] = value;

    this.orderObs.next(currentOrder);
  }
}
