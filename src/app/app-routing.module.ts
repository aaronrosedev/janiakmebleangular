import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { CategoryPageComponent } from './pages/category-page/category-page.component';
import { ProductPageComponent } from './pages/product-page/product-page.component';
import { OrderPageComponent } from './pages/order-page/order-page.component';
import { DeliveryPageComponent } from './pages/delivery-page/delivery-page.component';
import { InstallmentsPageComponent } from './pages/installments-page/installments-page.component';
import { WarrantyPageComponent } from './pages/warranty-page/warranty-page.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MainPageComponent
  },
  {
    path: 'transport',
    component: DeliveryPageComponent
  },
  {
    path: 'zakupy-na-raty',
    component: InstallmentsPageComponent
  },
  {
    path: 'gwarancja',
    component: WarrantyPageComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: 'zamowienie/:product',
    component: OrderPageComponent
  },
  {
    path: ':category',
    component: CategoryPageComponent
  },
  {
    path: 'meble/:category',
    redirectTo: ':category'
  },
  {
    path: ':category/:product',
    component: ProductPageComponent
  },
  {
    path: 'meble/produkt/:product',
    redirectTo: 'produkt/:product'
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
