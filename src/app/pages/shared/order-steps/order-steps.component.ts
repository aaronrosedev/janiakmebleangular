import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-order-steps',
  templateUrl: './order-steps.component.html',
  styleUrls: ['./order-steps.component.scss']
})
export class OrderStepsComponent implements OnInit {

  public steps = ['wybierz produkt', 'wybierz tkaninę', 'wybierz dodatki', 'oblicz koszt transportu', 'uzupełnij dane', 'potwierdź zamówienie'];

  @Input() currentStep = 1;

  constructor() { }

  ngOnInit() {
  }

  public getClass(i: number): string {
    if (i + 1 < this.currentStep)
      return 'done';

    if (i + 1 === this.currentStep)
      return 'current'

    return '';
  }

}
