import { Component, OnInit, HostListener, Inject } from '@angular/core';

import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ]
})
export class HeaderComponent implements OnInit {

  public navIsFixed: boolean = false;

  constructor(

  ) { }

  ngOnInit() {
  }

  onResized(event) {
    const width = event.newWidth;
    const height = event.newHeight;
    const element = document.getElementById('sticky-plug');

    if (!this.navIsFixed) {
      element.style.height = height.toString() + 'px';
      this.navIsFixed = true;
    }

  }


}
