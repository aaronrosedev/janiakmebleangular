import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { OfferService } from '../../services/offer.service';
import { ProductInCategory } from '../../models/product-in-category';
import { Category } from '../../models/category';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-category-page',
    templateUrl: './category-page.component.html',
    styleUrls: ['./category-page.component.scss']
})
export class CategoryPageComponent implements OnInit {

    public products: Array<ProductInCategory> = [{
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }, {
        id: 0,
        name: 'ładowanie...',
        price: null,
        img: 'assets/images/photo-loader.png',
        isMainProduct: true,
        seoUrl: ''
    }];
    private categroy: Category;

    public title = 'ładowanie... ';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private offerService: OfferService,
        private titleService: Title) { }

    async ngOnInit() {
        this.route.paramMap.subscribe(async (param: Params) => {
            const categoryName = param.get('category');

            this.categroy = await this.offerService.getCategory(categoryName);

            if (!this.categroy) {
                this.router.navigate(['404']);

                return;
            }

            this.title = this.categroy.name;
            this.products = await this.offerService.getProductsInCategory(categoryName);

            this.titleService.setTitle(this.title + ' - ' + 'Janiak Meble - producent mebli tapicerowanych w okolicy Wrocławia');

        });
    }

}
