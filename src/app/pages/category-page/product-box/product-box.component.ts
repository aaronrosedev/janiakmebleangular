import { Component, OnInit, Input } from '@angular/core';
import { ProductInCategory } from '../../../models/product-in-category';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: [ './product-box.component.scss' ]
})
export class ProductBoxComponent implements OnInit {

  @Input() product: ProductInCategory;

  constructor() { }

  ngOnInit() {
  }

}
