import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductInformationBoxesComponent } from './product-information-boxes.component';

describe('ProductInformationBoxesComponent', () => {
  let component: ProductInformationBoxesComponent;
  let fixture: ComponentFixture<ProductInformationBoxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductInformationBoxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInformationBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
