import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../models/product';
import { MatDialog } from '@angular/material/dialog';
import { OrderDialogComponent } from '../../../shared/dialogs/order-dialog/order-dialog.component';
import { MessageSendResultDialogComponent } from '../../../shared/dialogs/message-send-result/message-send-result-dialog.component';

@Component({
  selector: 'app-buy-box',
  templateUrl: './buy-box.component.html',
  styleUrls: [ './buy-box.component.scss' ]
})
export class BuyBoxComponent implements OnInit {

  @Input() product: Product;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  showOrderDialog(): void {
    const dialogRef = this.dialog.open(OrderDialogComponent, {
      autoFocus: false,
      data: this.product
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log(result);
      if (result !== void (0)) {
        this.dialog.open(MessageSendResultDialogComponent, {
          autoFocus: false,
          data: result
        });
      }
    });


  }
}
