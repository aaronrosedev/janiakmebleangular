/* eslint-disable */
import { Component, OnInit } from '@angular/core';
import { OfferService } from '../../services/offer.service';
import { Product } from '../../models/product';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';


@Component({
    selector: 'app-product-page',
    templateUrl: './product-page.component.html',
    styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {
    public product: Product = {
        images: [
            'assets/images/photo-loader.png',
            'assets/images/photo-loader.png',
            'assets/images/photo-loader.png',
            'assets/images/photo-loader.png',
            'assets/images/photo-loader.png'
        ],
        name: 'ładowanie...',
        id: 0,
        additions: [],
        attributes: [],
        category: { id: 0, name: '', photo: '', prefix: '', seo_url: '' },
        description: 'ładowanie...',
        parameters: [],
        seoUrl: '',
        price: 0,
        sizes: [],
        textilies: []
    };
    public breadcrumbs: { name: string, url: string }[];

    constructor(
        public dialog: MatDialog,
        private router: Router,
        private route: ActivatedRoute,
        private offerService: OfferService,
        private titleService: Title) { }

    async ngOnInit() {

        this.route.paramMap.subscribe(async (param: Params) => {
            const productName = param.get('product');
            this.offerService.getProduct(productName).then((product) => {


                if (!product) {
                    this.router.navigate(['404']);

                    return;
                }

                this.product = product;

                this.breadcrumbs = [
                    {
                        name: product.category.name,
                        url: '/' + product.category.seo_url
                    },
                    {
                        name: product.category.prefix + ' ' + product.name,
                        url: '/' + product.category.seo_url + '/' + product.seoUrl
                    },
                ];

                this.titleService.setTitle(product.category.prefix + ' ' + product.name + ' - ' + 'Janiak Meble - producent mebli tapicerowanych w okolicy Wrocławia');

                this.offerService.getTextiles();
            });

        });
    }
}
