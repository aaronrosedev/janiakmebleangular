import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PhotoPreviewComponent } from '../../../shared/dialogs/photo-preview/photo-preview.component';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: [ './photo-gallery.component.scss' ]
})
export class PhotoGalleryComponent implements OnInit {

  @Input() images: Array<string>;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  public showPrewiew(index: number) {
    this.dialog.open(PhotoPreviewComponent, {
      autoFocus: false,
      data: { index: index, photos: this.images }
    });
  }

}
