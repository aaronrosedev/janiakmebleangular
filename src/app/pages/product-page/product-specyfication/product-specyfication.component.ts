import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../models/product';
import { KeyValuePair } from '../../../models/helpers/key-value-pair';
import { OfferService } from '../../../services/offer.service';

@Component({
  selector: 'app-product-specyfication',
  templateUrl: './product-specyfication.component.html',
  styleUrls: [ './product-specyfication.component.scss' ]
})
export class ProductSpecyficationComponent implements OnInit {

  @Input() product: Product;

  public sizes: KeyValuePair[];
  public parameters: KeyValuePair[];
  public attributes: string[];


  constructor(private offerService: OfferService) {
    this.offerService.getSizes().subscribe(sizes => this.sizes = sizes);
    this.offerService.getParameters().subscribe(parameters => this.parameters = parameters);
    this.offerService.getAttributes().subscribe(attributes => this.attributes = attributes);

  }

  ngOnInit() {
  }

}
