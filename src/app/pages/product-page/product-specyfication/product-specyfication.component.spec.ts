import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSpecyficationComponent } from './product-specyfication.component';

describe('ProductSpecyficationComponent', () => {
  let component: ProductSpecyficationComponent;
  let fixture: ComponentFixture<ProductSpecyficationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSpecyficationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSpecyficationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
