import { Component, OnInit, Input } from '@angular/core';
import { Textile, TextileColor } from '../../../models/textile';
import { OrderService } from '../../../services/order.service';
import { OfferService } from '../../../services/offer.service';

@Component({
  selector: 'app-textile-select',
  templateUrl: './textile-select.component.html',
  styleUrls: ['./textile-select.component.scss']
})
export class TextileSelectComponent implements OnInit {

  public selectedTab: Textile;

  textilies: Array<Textile>;

  constructor(private orderService: OrderService, private offerService: OfferService) {
  }

  ngOnInit() {
    this.offerService.getTextilesObs().subscribe(textiles => {
      this.textilies = textiles;
      this.selectedTab = this.textilies[0];
    });

  }

  public selectTab(textile: Textile) {
    this.selectedTab = textile;
  }

  public colorSelect(selectedColor: TextileColor) {
    //console.log(selectedColor);
    this.orderService.selectTextile(this.selectedTab, selectedColor);
  }

}
