import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextileSelectComponent } from './textile-select.component';

describe('TextileSelectComponent', () => {
  let component: TextileSelectComponent;
  let fixture: ComponentFixture<TextileSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextileSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextileSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
