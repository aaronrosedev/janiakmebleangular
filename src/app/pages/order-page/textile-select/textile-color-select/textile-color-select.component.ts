import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Textile, TextileColor } from '../../../../models/textile';

@Component({
  selector: 'app-textile-color-select',
  templateUrl: './textile-color-select.component.html',
  styleUrls: [ './textile-color-select.component.scss' ]
})
export class TextileColorSelectComponent implements OnInit {

  @Input() textile: Textile;
  @Output() colorSelect = new EventEmitter<TextileColor>();

  selectedColor: TextileColor;

  constructor() { }

  ngOnInit() {
    this.selectedColor = this.textile.samples[ 0 ];
    this.colorSelect.emit(this.selectedColor);
  }

  onSelect(selectedColor: TextileColor) {
    //console.log(selectedColor);
    this.selectedColor = selectedColor;
    this.colorSelect.emit(selectedColor);
  }

}
