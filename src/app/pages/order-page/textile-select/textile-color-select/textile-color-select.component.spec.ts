import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextileColorSelectComponent } from './textile-color-select.component';

describe('TextileColorSelectComponent', () => {
  let component: TextileColorSelectComponent;
  let fixture: ComponentFixture<TextileColorSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextileColorSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextileColorSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
