import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: [ './personal-data.component.scss' ]
})
export class PersonalDataComponent implements OnInit {

  personalDataForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    emailAdress: new FormControl(''),
    phoneNumber: new FormControl(''),
    postcode: new FormControl(''),
    city: new FormControl(''),
    street: new FormControl(''),
    houseNumber: new FormControl('')
  });

  constructor() {
  }

  ngOnInit() {

  }

  onSubmit() {
    //console.log(this.personalDataForm);
  }

}
