import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { Order } from '../../../models/order';

@Component({
  selector: 'app-order-current-product',
  templateUrl: './order-current-product.component.html',
  styleUrls: [ './order-current-product.component.scss' ]
})
export class OrderCurrentProductComponent implements OnInit {

  public order: Order;

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.orderService.getOrderObs().subscribe(order => {
      this.order = order;
    });
  }
}
