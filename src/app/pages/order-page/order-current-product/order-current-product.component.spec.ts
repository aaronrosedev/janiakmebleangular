import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCurrentProductComponent } from './order-current-product.component';

describe('OrderCurrentProductComponent', () => {
  let component: OrderCurrentProductComponent;
  let fixture: ComponentFixture<OrderCurrentProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCurrentProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCurrentProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
