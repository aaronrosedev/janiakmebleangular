import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionsSelectComponent } from './additions-select.component';

describe('AdditionsSelectComponent', () => {
  let component: AdditionsSelectComponent;
  let fixture: ComponentFixture<AdditionsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionsSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
