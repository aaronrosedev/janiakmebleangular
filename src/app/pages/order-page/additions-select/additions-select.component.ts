import { Component, OnInit, Input } from '@angular/core';
import { Addition } from '../../../models/addition';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'app-additions-select',
  templateUrl: './additions-select.component.html',
  styleUrls: [ './additions-select.component.scss' ]
})
export class AdditionsSelectComponent implements OnInit {

  @Input() additions: Array<Addition>;

  private _currentSelectedAdditions: Array<Addition>;

  constructor(private _orderService: OrderService) { }

  ngOnInit() {
    this._orderService.getOrderObs().subscribe(currentOrder => this._currentSelectedAdditions = currentOrder.additions);
  }

  public addAddition(addition: Addition) {
    this._orderService.addAddition(addition);
  }

  public removeAddition(addition: Addition) {
    this._orderService.removeAddition(addition);
  }

  public isNowSelected(addition: Addition): boolean {
    return this._currentSelectedAdditions && this._currentSelectedAdditions.indexOf(addition) > -1;
  }

}
