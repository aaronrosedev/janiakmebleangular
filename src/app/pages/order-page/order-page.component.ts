import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { OfferService } from '../../services/offer.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../models/product';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: [ './order-page.component.scss' ]
})
export class OrderPageComponent implements OnInit {

  public product: Product;
  public currentStep: number;

  constructor(
    private route: ActivatedRoute,
    private offerService: OfferService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.currentStep = 2;

    this.route.paramMap.subscribe((param: Params) => {
      const productName = param.get('product');
      this.offerService.getProduct(productName).then((product) => {
        this.product = product;
        //console.log(product);
        this.orderService.selectProduct(product);
        this.offerService.getTextiles();
      });
    });

    this.orderService.getOrderObs().subscribe(product => this.currentStep = product ? product.currentStep : 2);
  }



}
