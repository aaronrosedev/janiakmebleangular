import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'app-order-buttons',
  templateUrl: './order-buttons.component.html',
  styleUrls: [ './order-buttons.component.scss' ]
})
export class OrderButtonsComponent implements OnInit {

  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit() {
  }

  public next() {
    this.orderService.nextStep();
  }

  public prev() {
    this.orderService.prevStep();
  }
}
