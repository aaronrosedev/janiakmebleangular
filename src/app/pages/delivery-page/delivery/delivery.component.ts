import { Component, OnInit } from '@angular/core';
import { DistanceService } from '../../../services/distance.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { OrderService } from '../../../services/order.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: [ './delivery.component.scss' ]
})
export class DeliveryComponent implements OnInit {

  public distance;
  public modelChanged: Subject<string> = new Subject<string>();

  public selectedDistanceThreshold;

  public distanceThresholds = [

    {
      from: 0,
      to: 50,
      price: 50
    },
    {
      from: 50,
      to: 100,
      price: 100
    },
    {
      from: 100,
      to: 150,
      price: 150
    },
    {
      from: 150,
      to: 250,
      price: 200
    },
    {
      from: 250,
      to: 99999999,
      price: 250
    }
  ];

  constructor(
    private distanceService: DistanceService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.modelChanged.pipe(
      debounceTime(300)).pipe( // wait 300ms after the last event before emitting last event
        distinctUntilChanged()) // only emit if value is different from previous value
      .subscribe(city => this.getDistance(city));

    this.orderService.getOrderObs().subscribe(order =>
      this.selectedDistanceThreshold = order ? order.delivery : null);
  }

  public getDistance(city: string) {
    this.distance = 'czekaj...';
    this.distanceService.getDistance(city).subscribe(response => {
      this.distance = response.route.distance * 1.609344;
      const calculatedDistanceThreshold =
        this.distanceThresholds
          .filter(A => this.distance > A.from && this.distance < A.to)[ 0 ];

      this.orderService.selectDeliveryCost(calculatedDistanceThreshold);
    });


  }

  changed(text: string) {
    this.modelChanged.next(text);
  }

}
