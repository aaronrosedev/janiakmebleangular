import { Component, OnInit } from '@angular/core';
import { ContactFormMessage } from '../../../models/contact-form-message';
import { ContactService } from '../../../services/contact.service';
import { MatDialog } from '@angular/material/dialog';
import { MessageSendResultDialogComponent } from '../../../shared/dialogs/message-send-result/message-send-result-dialog.component';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: [ './contact-form.component.scss' ]
})
export class ContactFormComponent implements OnInit {

  constructor(private contactService: ContactService, public dialog: MatDialog) { }

  public form: ContactFormMessage;

  ngOnInit() {
    this.form = new ContactFormMessage();
  }

  public onSubmit() {
    this.contactService.sendMessage(this.form).subscribe(res => this.showResultDialog(res));
  }

  showResultDialog(result: Boolean): void {
    const dialogRef = this.dialog.open(MessageSendResultDialogComponent, {
      autoFocus: false,
      data: result
    });
  }

}
