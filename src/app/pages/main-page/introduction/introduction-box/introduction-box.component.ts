import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-introduction-box',
  templateUrl: './introduction-box.component.html',
  styleUrls: ['./introduction-box.component.scss']
})
export class IntroductionBoxComponent implements OnInit {

  @Input()
  header: string = '';

  @Input()
  description: string = '';

  @Input()
  icon: string = '';

  @Input()
  url: string = '';

  constructor() { }

  ngOnInit() {
  }

}
