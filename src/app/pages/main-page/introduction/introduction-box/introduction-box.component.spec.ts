import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionBoxComponent } from './introduction-box.component';

describe('IntroductionBoxComponent', () => {
  let component: IntroductionBoxComponent;
  let fixture: ComponentFixture<IntroductionBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
