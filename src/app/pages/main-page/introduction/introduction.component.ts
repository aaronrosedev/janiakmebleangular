import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss']
})
export class IntroductionComponent implements OnInit {

  public boxes: Array<Box>;

  constructor() {
    this.boxes = [
      {
        header: 'Jesteśmy małą firmą',
        description: 'dzięki czemu mamy wgląd w cały proces produkcji naszych mebli.',
        icon: '/assets/images/teamwork.svg',
        url: ''
      },
      {
        header: 'Wykorzystujemy materiały dobrej jakości',
        description: 'sami wytwarzamy większość komponentów z których produkowane są nasze meble dzięki czemu wiemy, że są one trwałe na lata.',
        icon: '/assets/images/protection.svg',
        url: ''
      },
      {
        header: 'Dysponujemy własną dystrybucją',
        description: 'dzięki której wysoka jakość idzie w parze z przystępną ceną.',
        icon: '/assets/images/map.svg',
        url: 'transport'
      },
      {
        header: 'Jesteśmy elastyczni',
        description: 'dzięki temu, że projektujemy meble od zera i jesteśmy gotowi na wszelkie sugestie klientów.',
        icon: '/assets/images/idea.svg',
        url: ''
      },
    ]
  }

  ngOnInit() {
  }

}

export interface Box {
  header: string;
  description: string;
  icon: string;
  url: string;
}
