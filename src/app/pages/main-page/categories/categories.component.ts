import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Category } from '../../../models/category';
import { OfferService } from '../../../services/offer.service';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

    public categories: Category[] = [
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' },
        { seo_url: '', id: 0, name: 'ładowanie...', prefix: '', photo: 'assets/images/photo-loader.png' }
    ];

    public constructor(
        private offerService: OfferService
    ) { }

    public async ngOnInit(): Promise<void> {
        this.categories = await this.offerService.getCategories();

    }

}
