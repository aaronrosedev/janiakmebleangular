import { Component, OnInit } from '@angular/core';
import { OfferService } from '../../../services/offer.service';
import { ProductInCategory } from '../../../models/product-in-category';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.scss']
})
export class RecommendedComponent implements OnInit {


  public products: Array<ProductInCategory> = [{
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }, {
    id: 0,
    name: 'ładowanie...',
    price: null,
    img: 'assets/images/photo-loader.png',
    isMainProduct: true,
    seoUrl: ''
  }];

  constructor(
    private offerService: OfferService
  ) { }

  async ngOnInit() {
    this.products = await this.offerService.getRecommendedProducts(12);

    this.products.forEach(p => p.seoUrl = 'polecane/' + p.seoUrl);
  }

}
