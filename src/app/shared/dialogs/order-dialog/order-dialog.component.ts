import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '../../../models/product';
import { ContactService } from '../../../services/contact.service';
import { OrderFormMessage } from '../../../models/order-form-message';

@Component({
  selector: 'app-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: [ './order-dialog.component.scss' ]
})
export class OrderDialogComponent implements OnInit {

  ngOnInit(): void {
    this.form = new OrderFormMessage(this.data.name);
  }

  constructor(
    private contactService: ContactService,
    public dialogRef: MatDialogRef<OrderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product
  ) { }

  public form: OrderFormMessage;

  public onSubmit() {
    this.contactService.sendOrderMessage(this.form).subscribe(result => this.dialogRef.close(result));
  }

}
