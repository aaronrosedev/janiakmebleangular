import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-photo-preview',
  templateUrl: './photo-preview.component.html',
  styleUrls: [ './photo-preview.component.scss' ]
})
export class PhotoPreviewComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public images: { index: number, photos: string[] },

  ) { }

  ngOnInit() {
  }

  canMovePrev(index: number) {
    return index > 0;
  }

  canMoveNext(index: number) {
    return index < this.images.photos.length - 1;
  }

  movePrev() {
    if (this.canMovePrev(this.images.index))
      this.images.index--;
  }

  moveNext() {
    if (this.canMoveNext(this.images.index))
      this.images.index++;
  }

}
