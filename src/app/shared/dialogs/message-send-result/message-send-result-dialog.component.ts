import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-message-send-result',
  templateUrl: './message-send-result.component.html',
  styleUrls: ['./message-send-result.component.scss']
})
export class MessageSendResultDialogComponent implements OnInit {

  ngOnInit(): void {
  }

  constructor(
    public dialogRef: MatDialogRef<MessageSendResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Boolean
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
