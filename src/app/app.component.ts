import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, NavigationStart } from '@angular/router';
import { trigger, state, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  animations: [
    trigger('fade',
      [
        state('void', style({ opacity: 0 })),
        transition(':enter', [ animate(300) ]),
        transition(':leave', [ animate(500) ]),
      ]
    ) ]
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute) { }


  private _pageHeight;
  private debounce_timer;
  private _fragment;

  async ngOnInit() {

    this.route.fragment.subscribe((fragment: string) => {

      if (fragment) {
        this._fragment = fragment;
      } else {
        this._fragment = null;
      }
    });

    this.router.events.subscribe((evt) => {

      if (evt instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', evt.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');

      }

      if (!(evt instanceof NavigationEnd)) {
        return;
      }

      this._redirectToFragment();
    });
  }

  private _redirectToFragment() {
    let scroll = 0;

    if (this._fragment) {
      const fragment = document.getElementById(this._fragment);
      if (fragment) {
        scroll = fragment.offsetTop - 70;
      }
    }

    if (window) {
      window.scroll({
        top: scroll,
        left: 0,
        behavior: 'smooth'
      });
    }
  }

  @HostListener('window:scroll', [ '$event' ])
  onWindowScroll(e) {
    if (window) {

      this._pageHeight = document.body.offsetHeight - window.innerHeight;

      if (this.debounce_timer) {
        window.clearTimeout(this.debounce_timer);
      }

      this.debounce_timer = window.setTimeout(() => {
        this.refreshProgressBar();

        if (window.pageYOffset > 0) {
          const element = document.getElementById('navbar');
          element.classList.add('sticky');
        } else {
          const element = document.getElementById('navbar');
          element.classList.remove('sticky');
        }

      }, 100);
    }
  }

  private refreshProgressBar() {
    const percent = Math.floor(window.pageYOffset / this._pageHeight * 100);

    const progress = document.getElementById('progress-bar');
    progress.style.width = percent.toString() + '%';
  }

}
