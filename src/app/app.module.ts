import { ProductsService } from './services/products.service';
import { CategoriesService } from './services/categories.service';
import { GraphQlService } from './services/graphQl.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/shared/header/header.component';
import { FooterComponent } from './pages/shared/footer/footer.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { MenuComponent } from './pages/shared/header/menu/menu.component';
import { BanerComponent } from './pages/main-page/baner/baner.component';
import { SloganComponent } from './pages/main-page/slogan/slogan.component';
import { IntroductionComponent } from './pages/main-page/introduction/introduction.component';
import { IntroductionBoxComponent } from './pages/main-page/introduction/introduction-box/introduction-box.component';
import { CategoriesComponent } from './pages/main-page/categories/categories.component';
import { OfferService } from './services/offer.service';
import { DistanceService } from './services/distance.service';
import { ContactFormComponent } from './pages/main-page/contact-form/contact-form.component';
import { CategoryPageComponent } from './pages/category-page/category-page.component';
import { ProductBoxComponent } from './pages/category-page/product-box/product-box.component';
import { PageTitleComponent } from './shared/page-title/page-title.component';
import { ProductPageComponent } from './pages/product-page/product-page.component';
import { BuyBoxComponent } from './pages/product-page/buy-box/buy-box.component';
import { OrderStepsComponent } from './pages/shared/order-steps/order-steps.component';
import { PhotoGalleryComponent } from './pages/product-page/photo-gallery/photo-gallery.component';
import { ProductSpecyficationComponent } from './pages/product-page/product-specyfication/product-specyfication.component';
import { ProductInformationBoxesComponent } from './pages/product-page/product-information-boxes/product-information-boxes.component';
import { OrderPageComponent } from './pages/order-page/order-page.component';
import { OrderCurrentProductComponent } from './pages/order-page/order-current-product/order-current-product.component';
import { OrderButtonsComponent } from './pages/order-page/order-buttons/order-buttons.component';
import { TextileSelectComponent } from './pages/order-page/textile-select/textile-select.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TextileColorSelectComponent } from './pages/order-page/textile-select/textile-color-select/textile-color-select.component';
import { AdditionsSelectComponent } from './pages/order-page/additions-select/additions-select.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { PersonalDataComponent } from './pages/order-page/personal-data/personal-data.component';
import { OrderDialogComponent } from './shared/dialogs/order-dialog/order-dialog.component';
import { MessageSendResultDialogComponent } from './shared/dialogs/message-send-result/message-send-result-dialog.component';
import { DeliveryComponent } from './pages/delivery-page/delivery/delivery.component';
import { DeliveryPageComponent } from './pages/delivery-page/delivery-page.component';
import { InstallmentsPageComponent } from './pages/installments-page/installments-page.component';
import { WarrantyPageComponent } from './pages/warranty-page/warranty-page.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PhotoPreviewComponent } from './shared/dialogs/photo-preview/photo-preview.component';
import { RecommendedComponent } from './pages/main-page/recommended/recommended.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainPageComponent,
    MenuComponent,
    BanerComponent,
    SloganComponent,
    IntroductionComponent,
    IntroductionBoxComponent,
    CategoriesComponent,
    ContactFormComponent,
    CategoryPageComponent,
    ProductBoxComponent,
    PageTitleComponent,
    ProductPageComponent,
    BuyBoxComponent,
    ProductPageComponent,
    OrderStepsComponent,
    PhotoGalleryComponent,
    ProductSpecyficationComponent,
    ProductInformationBoxesComponent,
    OrderPageComponent,
    OrderCurrentProductComponent,
    OrderButtonsComponent,
    TextileSelectComponent,
    TextileColorSelectComponent,
    AdditionsSelectComponent,
    DeliveryComponent,
    PersonalDataComponent,
    OrderDialogComponent,
    MessageSendResultDialogComponent,
    PhotoPreviewComponent,
    DeliveryPageComponent,
    InstallmentsPageComponent,
    WarrantyPageComponent,
    NotFoundComponent,
    RecommendedComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  providers: [
    OfferService,
    DistanceService,
    GraphQlService,
    CategoriesService,
    ProductsService
  ],
  entryComponents: [
    OrderDialogComponent,
    MessageSendResultDialogComponent,
    PhotoPreviewComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
